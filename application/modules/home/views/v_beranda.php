<?php 
 $user  = $this->session->userdata('sess_login');

 //var_dump($user);die();
?>
<section id="intro">
<div class="container">
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-8">
    <div class="block">
      <div class="section-title">
        <h2>Hai, <?php echo $user["nm_depan"].' '.$user["nm_belakang"]; ?> !</h2>
        <h6>Selamat datang di Sistem Registrasi Online Ubharajaya!</h6>
        <p>
            Universitas Bhayangkara Jakarta Raya (Ubhara Jaya) sebagai salah satu
            Perguruan Tinggi Swasta yang berada dibawah pembinaan Yayasan Brata Bhakti sebagai badan penyelenggaranya,
            berkewajiban mewujudkan tujuan pendidikan nasional dengan Visi dan Misi nya bagi mendukung keberhasilan tugas 
            Kepolisian Negara Republik Indonesia khususnya dan pengembangan kualitas hidup bermasyarakat berbangsa dan benegara pada umumnya.
        </p>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<br>

<div class="row">
  <div class="col-md-2"></div>
      <div class="col-sm-8">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Kegiatan</th>
              <th>Pelaksanaan</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
</div>