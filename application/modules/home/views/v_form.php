<script>
    $(document).ready(function(){

        // pilih prodi
        $('#prog').change(function(){
            $.post('<?php echo base_url()?>home/get_jurusan/'+$(this).val(),{},function(get){
                $('#prod').html(get);
            });
        });

        // kewarganegaraan
        $('#kwn').hide();
        $('#wni').click(function () {
            $('#kwn').hide();
        });

        $('#wna').click(function () {
            $('#kwn').show();
        });

        // tanggal lahir
        // $('#tgl_lhr').datepicker({
        //     dateFormat: "yy-mm-dd",
        //     yearRange: "1945:2017",
        //     changeMonth: true,
        //     changeYear: true

        // });

        // status kerja
        $('#stsb_txt').hide();
        $('#stsb_n').click(function () {
            $('#stsb_txt').hide();
        });

        $('#stsb_y').click(function () {
            $('#stsb_txt').show();
        });

        // for form pasca
        $('#optmaba2').hide();
        $('#formpasca').hide();
        $('#jenis1').hide();
        $('#jenis2').hide();
        $('#strata1').click(function () {
            $('#formpasca').hide();
            $('#optmaba1').show();
            $('#optmaba2').hide();
            $('#jenis1').show();
            $('#jenis2').hide();
        });
        $('#strata2').click(function () {
            $('#formpasca').show();
            $('#optmaba1').hide();
            $('#optmaba2').show();
            $('#jenis1').hide();
            $('#jenis2').show();
        });

        // program
        $('#konversi').hide();
        $('#new0').hide();
        $('#readmisi').hide();

        $('#r').click(function () {
            $('#konversi').hide();
            $('#new0').hide();
            $('#readmisi').show();
        });
        $('#new').click(function () {
            $('#konversi').hide();
            $('#new0').show();
            $('#readmisi').hide();
        });
        $('#k').click(function () {
            $('#konversi').show();
            $('#new0').hide();
            $('#readmisi').hide();
        });

        // kelengkapan
        $('#spd').hide();
        $('#sak').hide();
        $('#tkr').hide(); 
        $('#baa').hide();
        $('#ketren').hide();

        $('#k').click(function () {
            $('#spd').show();
            $('#sak').show();
            $('#tkr').show();
            $('#skhun').hide();
            $('#skl').hide();
            $('#rpt').hide();
        });

        $('#r').click(function () {
            $('#baa').show();
            $('#tkr').show(); 
            $('#ketren').show();
            $('#ijz').hide();
            $('#skhun').hide();
            $('#skl').hide();
            $('#rpt').hide();
        });

        $('#new').click(function () {
            $('#spd').hide();
            $('#sak').hide();
            $('#tkr').hide();
            $('#baa').hide();
            $('#ketren').hide();
            $('.dda').show();
        });

        // bpjs
        $('#bpjs-yes').hide();
        $('#bpjs-y').click(function () {
            $('#bpjs-yes').show();
        }); 

        $('#bpjs-n').click(function () {
            $('#bpjs-yes').hide();
        }); 

        // ubah jenis untuk s2
        $('#mabas2').hide();
        $('#reads2').hide();
        $('#konvs2').hide();
        $('#n2').click(function () {
            $('#mabas2').show();
            $('#reads2').hide();
            $('#konvs2').hide();
        });
        $('#r2').click(function () {
            $('#mabas2').hide();
            $('#reads2').show();
            $('#konvs2').hide();
        });
        $('#k2').click(function () {
            $('#mabas2').hide();
            $('#reads2').hide();
            $('#konvs2').show();
        }); 

    });
</script>
<div class="col-sm-8 col-sm-offset-2">
    <!--      Wizard container        -->
    <div class="wizard-container">
        <div class="card wizard-card" data-color="green" id="wizardProfile">
            <form action="<?php echo base_url(); ?>home/add_form" method="post" onsubmit="simpan.disabled = true; simpan.value='Please wait ..'; return true;">
            <!--You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                <div class="wizard-header">
                    <h3 class="wizard-title">
                       Mohon Lengkapi Formulir Anda
                    </h3>
                    <small>Informasi ini akan digunakan untuk keperluan universitas terhadap calon pendaftar</small>
                </div>
                <div class="wizard-navigation">
                    <ul>
                        <li><a href="#prodi" data-toggle="tab">Pilihan Program Studi</a></li>
                        <li><a href="#about" data-toggle="tab">Data Pribadi</a></li>
                        <li><a href="#account" data-toggle="tab">Data Orang Tua</a></li>
                        <li><a href="#address" data-toggle="tab">Kelengkapan Data</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <!-- pilihan program -->
                    <div class="tab-pane" id="prodi">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Program <small>(required)</small></label>
                                        <select name="program" id="prog" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="1" id="strata1">Strata Satu (S1)</option>
                                            <option value="2" id="strata2">Pasca Sarjana (S2)</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">face</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Pendaftaran <small>(required)</small></label>
                                        <select name="jenisdaftar" class="form-control" id="optmaba1">
                                            <option selected="" disabled=""></option>
                                            <option value="MB" id="new">Mahasiswa Baru</option>
                                            <option value="RM" id="r">Mahasiswa Readmisi</option>
                                            <option value="KV" id="k">Mahasiswa Konversi</option>
                                        </select>
                                        <select name="jenisdaftar" class="form-control" id="optmaba2">
                                            <option selected="" disabled=""></option>
                                            <option value="MB" id="n2">Mahasiswa Baru</option>
                                            <option value="RM" id="r2">Mahasiswa Readmisi</option>
                                            <option value="KV" id="k2">Mahasiswa Konversi</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">watch_later</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Opsi Kelas <small>(required)</small></label>
                                        <select name="kelas" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="PG">Pagi</option>
                                            <option value="SR">Sore</option>
                                            <option value="KY">Karyawan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">place</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Lokasi Kampus <small>(required)</small></label>
                                        <select name="lokasikampus" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="JKT">Jakarta</option>
                                            <option value="BKS">Bekasi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">menu</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Program Studi <small>(required)</small></label>
                                        <select name="prodi" id="prod" class="form-control">
                                            <option selected="" disabled=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <!-- jenis maba untuk s1 -->
                            <div id="jenis1">
                                <!-- readmisi -->
                                <div id="readmisi">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM <small>(required)</small></label>
                                                <input name="npm_readmisi" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Masuk UBJ</label>
                                                    <input type="text" class="form-control" name="thmasuk_readmisi">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Sampai Dengan Semester</label>
                                                    <input type="text" class="form-control" id="" name="smtr_readmisi">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- baru -->
                                <div id="new0">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Asal Sekolah <small>(required)</small></label>
                                                <input name="asal_sch" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NISN <small>(required)</small></label>
                                                <input name="nisn" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">pin_drop</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota Asal Sekolah <small>(required)</small></label>
                                                <input name="kota_sch" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kelurahan Asal Sekolah <small>(required)</small></label>
                                                <input name="daerah_sch" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kategori Sekolah <small>(required)</small></label>
                                                <input type="radio" name="jenis_skl" value="NGR" > NEGERI &nbsp;&nbsp;
                                                <input type="radio" name="jenis_skl" value="SWT" > SWASTA &nbsp;&nbsp; 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">school</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Jenis Sekolah <small>(required)</small></label>
                                                <input type="radio" name="jenis_sch_maba" value="SMA" > SMA &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="SMK" > SMK &nbsp;&nbsp; 
                                                <input type="radio" name="jenis_sch_maba" value="MDA" > MA  &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="SMB" > SMTB  &nbsp;&nbsp;
                                                <input type="radio" name="jenis_sch_maba" value="OTH" > Lainnya
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">near_me</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Jurusan</label>
                                                    <input type="text" class="form-control" name="jur_sch">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Lulus</label>
                                                    <input type="text" class="form-control" id="" name="lulus_sch">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- konversi -->
                                <div id="konversi">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nama Perguruan Tinggi <small>(required)</small></label>
                                                <input name="asal_pts" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota PTN/PTS <small>(required)</small></label>
                                                <input name="kota_pts" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Program Studi <small>(required)</small></label>
                                                <input name="prodi_pts" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Tahun Lulus</label>
                                                    <input type="text" class="form-control" name="lulus_pts">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Semester</label>
                                                    <input type="text" class="form-control" id="" name="smtr_pts">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM/NIM Asal <small>(required)</small></label>
                                                <input name="npm_pts" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- konversi end -->
                            </div>
                            <!-- end jenis maba untuk s1 -->

                            <!-- jenis pendaftaran untuk s2 -->
                            <div id="jenis2">
                                <!-- maba start -->
                                <div id="mabas2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Asal Universitas</label>
                                                <input class="form-control span4" type="text" name="asal_sch">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- maba end -->

                                <!-- readmisi start -->
                                <div id="reads2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM Lama</label>
                                                <input class="form-control span4" type="text" name="npmsatu">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Tahun Masuk di UBJ</label>
                                                <input class="form-control span4" type="text" name="tahunmasuks2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- readmisi end -->

                                <!-- konversi start -->
                                <div id="konvs2">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Jenis Perguruan Tinggi</label>
                                                <input type="radio" name="jenis_skl" value="NGR" > NEGERI &nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="jenis_skl" value="SWT"> SWASTA
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">domain</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nama Perguruan Tinggi</label>
                                                <input class="form-control span4" type="text" name="asal_pts">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">local_library</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Program Studi</label>
                                                <input class="form-control span4" type="text" name="prodi_pts">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">today</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Tahun lulus/Semester</label>
                                                <input class="form-control span4" type="text" name="lulus_pts">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">map</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Kota Asal PTS/PTN</label>
                                                <input class="form-control span4" type="text" name="kota_pts">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">NPM/NIM</label>
                                                <input class="form-control span4" type="text" name="npm_pts">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- konversi end -->
                            </div>
                            <!-- end jenis pendaftaran untuk s2 -->

                            <!-- form pascasarjana -->
                            <!-- <div id="formpasca">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">credit_card</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">NPM strata satu (S1) <small> (required)</small></label>
                                          <input type="text"  name="npmsatu" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-group label-floating">
                                          <label class="control-label">Tahun Masuk UBJ <small> (required)</small></label>
                                          <input type="text"  name="tahunmasuks2" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- form pascasarjana end -->
                        </div>
                    </div>

                    <!-- data pribadi -->
                    <div class="tab-pane" id="about">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">NIK <small>Nomor Induk Kependudukan (required)</small></label>
                                      <input name="nik" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">perm_identity</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Nama Lengkap <small>Sesuai Ijazah Terakhir (required)</small></label>
                                      <input name="nama" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">today</i>
                                    </span>
                                    <div class="col-sm-8">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tempat Lahir</label>
                                            <input type="text" class="form-control" name="tpt_lahir">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <input type="text" class="form-control" id="tgl_lhr" name="tgl_lahir">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">people</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jenis Kelamin <small>(required)</small></label>
                                        <input  type="radio" name="jk" id="" value="L" required> Laki - Laki &nbsp;&nbsp;
                                        <input  type="radio" name="jk" id="" value="P" required> Perempuan
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">home</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alamat <small>(required)</small></label>
                                        <textarea name="alamat" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">mail</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kode Pos <small>(required)</small></label>
                                        <input name="kdpos" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">pin_drop</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Kewarganegaraan <small>(required)</small></label><br>
                                        <input type="radio" name="wn" id="wna" value="WNA" onclick="wna()" required> WNA &nbsp;&nbsp;
                                        <input type="radio" name="wn" id="wni" value="WNI" onclick="wni()" required> WNI
                                        <input id="kwn" name="wn_txt" placeholder="Kewarganegaraan Calon Mahasiswa (jika WNA)" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">brightness_low</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Agama <small>(required)</small></label>
                                        <select name="agama" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="ISL">Islam</option>
                                            <option value="KTL">Katolik</option>
                                            <option value="PRT">Protestan</option>
                                            <option value="BDH">Budha</option>
                                            <option value="HND">Hindu</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">wc</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Nikah <small>(required)</small></label>
                                        <input type="radio" name="stsm" value="Y" required> Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" value="N" required> Belum Menikah &nbsp;&nbsp;
                                        <input type="radio" name="stsm" value="D" required> Janda/Duda
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">domain</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Bekerja <small>(required)</small></label><br>
                                        <input type="radio" name="stsb" id="stsb_n" value="N" required> Belum Bekerja &nbsp;&nbsp;
                                        <input type="radio" name="stsb" id="stsb_y" value="Y"> Bekerja &nbsp;&nbsp; 
                                        <input type="text" class="form-control span3" id="stsb_txt"  name="stsb_txt" placeholder="Tempat Bekerja Calon Mahasiswa (jika sudah bekerja)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_iphone</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">Telepon / HP <small>nomor aktif (required)</small></label>
                                      <input type="text"  name="tlp" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_in_talk</i>
                                    </span>
                                    <div class="form-group label-floating">
                                      <label class="control-label">No. Telpon / HP Wali <small>nomor aktif (required)</small></label>
                                      <input type="text"  name="tlp2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- data orang tua -->
                    <div class="tab-pane" id="account">
                        <div class="row">
                            <!-- detail ayah -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ayah <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ayah">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ayah <small>(required)</small></label>
                                        <select name="didik_ayah" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="NSD">Tidak tamat SD</option>
                                            <option value="YSD">Tamat SD</option>
                                            <option value="SMP">Tamat SLTP</option>
                                            <option value="SMA">Tamat SLTA</option>
                                            <option value="DPL">Diploma</option>
                                            <option value="SMD">Sarjana Muda</option>
                                            <option value="SRJ">Sarjana</option>
                                            <option value="PSC">Pascasarjana</option>
                                            <option value="DTR">Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ayah <small>(required)</small></label>
                                        <select name="workdad" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="PN">Pegawai Negeri</option>
                                            <option value="TP">TNI / POLRI</option>
                                            <option value="PS">Pegawai Swasta</option>
                                            <option value="WU">Wirausaha</option>
                                            <option value="PS">Pensiun</option>
                                            <option value="TK">Tidak Bekerja</option>
                                            <option value="LL">Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ayah <small>(required)</small></label>
                                        <select name="life_statdad" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="MH">Masih Hidup</option>
                                        <option value="SM">Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- detail ayah /end -->
                            <hr>
                            <!-- detail ibu -->
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Ibu <small>(required)</small></label>
                                        <input class="form-control" type="text" name="nm_ibu">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">school</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pendidikan Ibu <small>(required)</small></label>
                                        <select name="didik_ibu" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="NSD">Tidak tamat SD</option>
                                            <option value="YSD">Tamat SD</option>
                                            <option value="SMP">Tamat SLTP</option>
                                            <option value="SMA">Tamat SLTA</option>
                                            <option value="DPL">Diploma</option>
                                            <option value="SMD">Sarjana Muda</option>
                                            <option value="SRJ">Sarjana</option>
                                            <option value="PSC">Pascasarjana</option>
                                            <option value="DTR">Doctor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">work</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pekerjaan Ibu <small>(required)</small></label>
                                        <select name="workmom" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="PN">Pegawai Negeri</option>
                                            <option value="TP">TNI / POLRI</option>
                                            <option value="PS">Pegawai Swasta</option>
                                            <option value="WU">Wirausaha</option>
                                            <option value="PS">Pensiun</option>
                                            <option value="TK">Tidak Bekerja</option>
                                            <option value="LL">Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">timeline</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Status Hidup Ibu <small>(required)</small></label>
                                        <select name="life_statmom" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="MH">Masih Hidup</option>
                                        <option value="SM">Sudah Meninggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jumlah Penghasilan Orang Tua <small>(required)</small></label>
                                        <input type="radio" value="1" name="gaji" required> Rp 1,000,000 - 2,000,000 &nbsp;&nbsp;
                                    
                                        <input type="radio" value="2" name="gaji"> Rp 2,100,000 - 4,000,000 <br>
                                    
                                        <input type="radio" value="3" name="gaji"> Rp 4,100,000 - 5,999,000  &nbsp;&nbsp;
                                
                                        <input type="radio" value="4" name="gaji"> >= Rp 6,000,000
                                    </div>
                                </div>
                            </div>
                            <!-- detail ibu /end -->
                        </div>
                    </div>

                    <!-- kelengkapan data -->
                    <div class="tab-pane" id="address">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">credit_card</i>
                                    </span>
                                    <div class="form-group">
                                        <label class="control-label">Pengguna BPJS <small>(required)</small></label><br>
                                        <input type="radio" id="bpjs-y" name="bpjs" value="y" required> Ya &nbsp;&nbsp;
                                        <input type="radio" id="bpjs-n" name="bpjs" value="n" > Tidak &nbsp;&nbsp; 
                                        <input class="form-control" id="bpjs-yes" type="text" placeholder="Nomor BPJS" name="nobpjs">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">motorcycle</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alat Transportasi <small>(required)</small></label>
                                        <select name="transportasi" id="" class="form-control">
                                            <option selected="" disabled=""></option>
                                            <option value="MBL">Mobil</option>
                                            <option value="MTR">Motor</option>
                                            <option value="AKT">Angkutan Umum</option>
                                            <option value="SPD">Sepeda</option>
                                            <option value="JKK">Jalan Kaki</option>
                                            <option value="ADG">Andong</option>
                                            <option value="KRT">Kereta</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">question_answer</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Referensi Memilih UBJ <small>(required)</small></label>
                                        <input class="form-control" id="" type="text" name="refer">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">assignment</i>
                                    </span>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Surat Kelengkapan <small>(required)</small></label>
                                        <div class="dda" id="akt"><input type="checkbox" name="lengkap[]" value="AKT"> Akte Kelahiran &nbsp;&nbsp;</div>
                                        <div class="dda" id="kk"><input type="checkbox" name="lengkap[]" value="KK"> Kartu Keluarga (KK) &nbsp;&nbsp; </div>
                                        <div class="dda" id="ktp"><input type="checkbox" name="lengkap[]" value="KTP"> Kartu Tanda Penduduk (KTP)  &nbsp;&nbsp;</div>
                                        <div class="dda" id="rpt"><input type="checkbox" name="lengkap[]" value="RP"> Rapot  &nbsp;&nbsp;</div>
                                        <div class="dda" id="skhun"><input type="checkbox" name="lengkap[]" value="SKHUN"> SKHUN  </div>
                                        <div class="dda" id="foto"><input type="checkbox" name="lengkap[]" value="FT"> Foto (3x4 dan 4x6) &nbsp;&nbsp;</div>
                                        <div class="dda" id="ijz"><input type="checkbox" name="lengkap[]" value="IJZ"> Ijazah &nbsp;&nbsp; </div>
                                        <div class="dda" id="skl"><input type="checkbox" name="lengkap[]" value="SKL"> Surat Kelulusan  &nbsp;&nbsp;<br></div>
                                        
                                        <div id="spd"><input type="checkbox" name="lengkap[]" value="SPD"> Surat Pindah &nbsp;&nbsp;</div>
                                        <div id="sak"><input type="checkbox" name="lengkap[]" value="SAK"> Sertifikat Akreditasi &nbsp;&nbsp;</div>

                                        <div id="tkr"><input type="checkbox" name="lengkap[]" value="TKR"> Transkrip  &nbsp;&nbsp;</div>
                                        <div id="baa"><input type="checkbox" name="lengkap[]" value="LBAA"> Laporan BAA  &nbsp;&nbsp;</div>
                                        <div id="ketren"><input type="checkbox" name="lengkap[]" value="KTREN"> Keterangan RENKEU </div>

                                        <div class="dda" id="lkp"><input type="checkbox" name="lengkap[]" value="LLKP"> Lengkap Administratif  &nbsp;&nbsp;</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="wizard-footer">
                    <div class="pull-right">
                        <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                        <input type='submit' id="simpan" class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' />
                    </div>

                    <div class="pull-left">
                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div> <!-- wizard container -->
</div>
