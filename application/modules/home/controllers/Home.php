<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('temph_model');
		$this->load->library('Cfpdf');
		$this->dbsia = $this->load->database('pmb', TRUE);
		$log = $this->session->userdata('sess_login');
		$array = array(
			'sess_login' => $log
		);
		
		$this->session->set_userdata( $array );
		// if ($this->session->userdata('sess_login') !=  TRUE || $this->session->userdata('sess_log_maba') !=  TRUE) {
		// 	echo "<script>alert('Menyingkir Kau Penyusup!');</script>";
		// 	redirect(base_url('board/login/out'),'refresh');
		// }
	}

	function index()
	{
		
		$data['page'] = 'v_beranda';
		$this->load->view('template',$data);
	}

	function form()
	{
		$log = $this->session->userdata('sess_login');
		$avb = $this->temph_model->getdetail('tbl_form_pmb','user_input',$log['userid'])->num_rows();
		if ($avb > 0) {
			redirect(base_url('home/load_form.'.$log['userid']),'refresh');
		} else {
			$data['loads1'] = $this->dbsia->query("SELECT * from tbl_jurusan_prodi where kd_fakultas != '6'")->result();
			$data['loads2'] = $this->dbsia->query("SELECT * from tbl_jurusan_prodi where kd_fakultas = '6'")->result();
			$data['page'] = 'v_form';
			$this->load->view('template',$data);
		}
		
	}

	function get_jurusan($id)
	{
		if ($id == 1) {
			$jurusan = $this->dbsia->query('SELECT * from tbl_jurusan_prodi where kd_fakultas != 6')->result();
			$out = "<select class='form-control' name='jurusan' id='prodi' selected='' disabled=''><option></option>";
	        foreach ($jurusan as $row) {
	            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
	        }
	        $out .= "</select>";
		} else {
			$jurusan = $this->dbsia->query('SELECT * from tbl_jurusan_prodi where kd_fakultas = 6')->result();
			$out = "<select class='form-control' name='jurusan' id='prodi' selected='' disabled=''><option></option>";
	        foreach ($jurusan as $row) {
	            $out .= "<option value='".$row->kd_prodi."'>".$row->prodi. "</option>";
	        }
	        $out .= "</select>";
		}
        echo $out;
	}

	function test_card($id)
	{
		$data['load'] = $this->temph_model->load_for_card($id)->row();
		$this->load->view('pdf_testcard', $data);
	}

	function add_form()
	{
		error_reporting(0);
		extract(PopulateForm());

		$logged = $this->session->userdata('sess_login');
		$log 	= $logged['userid'];

		$wave = $this->dbsia->query("SELECT * from tbl_gelombang_pmb where status = 1")->row();
		$gelombang = $wave->gelombang;
		$form = $this->input->post('jenisdaftar');
		// jika pendaftar s1
		if ($program == '1') {
			
			if ($form == 'RM') {
				$b = date('Y');
				$c = substr($b, 2,4);

				$q = $this->db->query('SELECT count(*) as jml from tbl_form_pmb where nomor_registrasi like "%'.$c.'.RA.S1%"')->row();
				$no = $q->jml;
				$no++;

				if($no <= 9){
	                $doc = $c.".RA.S1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".RA.S1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".RA.S1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".RA.S1.".$gelombang.".".$no;
	            }

			} elseif ($form == 'MB') {
				$b = date('Y');
				$c = substr($b, 2,4);

				$q = $this->db->query('SELECT count(*) as jml from tbl_form_pmb where nomor_registrasi like "%'.$c.'.S1%"')->row();
				$no = $q->jml;
				$no++;

				if($no <= 9){
	                $doc = $c.".S1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".S1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".S1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".S1.".$gelombang.".".$no;
	            }

			} elseif ($form == 'KV') {
				$b = date('Y');
				$c = substr($b, 2,4);

				$q = $this->db->query('SELECT count(*) as jml from tbl_form_pmb where nomor_registrasi like "%'.$c.'.KS1%"')->row();
				$no = $q->jml;
				$no++;

				if($no <= 9){
	                $doc = $c.".KS1.".$gelombang.".000".$no;
	            }elseif($no <= 99){
	                $doc = $c.".KS1.".$gelombang.".00".$no;
	            }elseif($no <= 999){
	                $doc = $c.".KS1.".$gelombang.".0".$no;
	            }elseif($no <= 9999){
	                $doc = $c.".KS1.".$gelombang.".".$no;
	            }
			}
		// jika pendaftar s2
		} else {

			if ($form == 'RM') {
				$t = date('Y');
				$y = substr($t, 2,4);
				$q = $this->db->query('SELECT count(*) as jmlh from tbl_form_pmb where nomor_registrasi like "%'.$y.'.RAS2%"')->row();
				$m = $q->jmlh;
				$m++;
				if($m<=9){
	                $doc = $y.".RAS2.".$gelombang.".000".$m;
	            }elseif($m<=99){
	                $doc = $y.".RAS2.".$gelombang.".00".$m;
	            }elseif($m<=999){
	                $doc = $y.".RAS2.".$gelombang.".0".$m;
	            }elseif($m<=9999) {
	                $doc = $y.".RAS2.".$gelombang.".".$m;
	            }

			} elseif($form == 'MB') {
				$t = date('Y');
				$y = substr($t, 2,4);
				$q = $this->db->query('SELECT count(*) as jmlh from tbl_form_pmb where nomor_registrasi like "%'.$y.'.S2%"')->row();
				$m = $q->jmlh;
				$m++; 
				if($m<=9){
	                $doc = $y.".S2.".$gelombang.".000".$m;
	            }elseif($m<=99){
	                $doc = $y.".S2.".$gelombang.".00".$m;
	            }elseif($m<=999){
	                $doc = $y.".S2.".$gelombang.".0".$m;
	            }elseif($m<=9999) {
	                $doc = $y.".S2.".$gelombang.".".$m;
	            }

			} elseif($form == 'KV') {
				$t = date('Y');
				$y = substr($t, 2,4);
				$q = $this->db->query('SELECT count(*) as jmlh from tbl_form_pmb where nomor_registrasi like "%'.$y.'.KS2%"')->row();
				$m = $q->jmlh;
				$m++;
				if($m<=9){
	                $doc = $y.".KS2.".$gelombang.".000".$m;
	            }elseif($m<=99){
	                $doc = $y.".KS2.".$gelombang.".00".$m;
	            }elseif($m<=999){
	                $doc = $y.".KS2.".$gelombang.".0".$m;
	            }elseif($m<=9999) {
	                $doc = $y.".KS2.".$gelombang.".".$m;
	            }
			}
		}

		$data = array(
				'program'				=> $program,
				'nomor_registrasi'		=> $doc,
				'nik'					=> $nik,
				'jenis_pmb'				=> $jenisdaftar,
				'kampus'				=> $lokasikampus,
				'keterangan'			=> '',
				'kelas'					=> $kelas,
				'nama'					=> strtoupper($nama),
				'kelamin'				=> $jk,
				'status_wn'				=> $wn,
				'kewarganegaraan'		=> strtoupper($wn_txt),
				'tempat_lahir'			=> strtoupper($tpt_lahir),
				'tgl_lahir'				=> $tgl_lahir,
				'agama'					=> $agama,
				'status_nikah'			=> $stsm,
				'status_kerja'			=> $stsb,
				'pekerjaan'				=> strtoupper($stsb_txt),
				'alamat'				=> $alamat,
				'kd_pos'				=> $kdpos,
				'tlp'					=> $tlp,
				'tlp2'					=> $tlp2,
				'npm_lama_readmisi'		=> $npm_readmisi,
				'tahun_masuk_readmisi'	=> $thmasuk_readmisi,
				'smtr_keluar_readmisi'	=> $smtr_readmisi,
				'asal_sch_maba'			=> $asal_sch,
				'daerah_sch_maba'		=> $daerah_sch,
				'kota_sch_maba'			=> $kota_sch,
				'jenis_sch_maba'		=> $jenis_sch_maba,
				'jur_maba'				=> $jur_sch,
				'lulus_maba'			=> $lulus_sch,
				'asal_pts_konversi'		=> $asal_pts,
				'kota_pts_konversi'		=> $kota_pts,
				'prodi_pts_konversi'	=> $prodi_pts,
				'npm_pts_konversi'		=> $npm_pts,
				'tahun_lulus_konversi'	=> $lulus_pts,
				'smtr_lulus_konversi'	=> $smtr_pts,
				'prodi'					=> $prodi,
				'nm_ayah'				=> strtoupper($nm_ayah),
				'didik_ayah'			=> $didik_ayah,
				'workdad'				=> $workdad,
				'life_statdad'			=> $life_statdad,
				'nm_ibu'				=> strtoupper($nm_ibu),
				'didik_ibu'				=> $didik_ibu,
				'workmom'				=> $workmom,
				'life_statmom'			=> $life_statmom,
				'penghasilan'			=> $gaji,
				'referensi'				=> $refer,
				'tanggal_regis'			=> date('Y-m-d H:i:s'),
				'gelombang'				=> $gelombang,
				'status'				=> '',
				'gelombang_du'			=> '',
				'npm_baru'				=> '',
				'foto'					=> '',
				'status_kelengkapan'	=> '', // $lengkap,
				'status_feeder'			=> '',
				'nisn'					=> $nisn,
				'kategori_skl'			=> $jenis_skl,
				'bpjs'					=> $bpjs,
				'no_bpjs'				=> $nobpjs,
				'user_input'			=> $log,
				'user_renkeu'			=> '',
				'user_baa'				=> '',
				'transport'				=> $transportasi,
				'npm_lama_s2'			=> $npmsatu,
				'th_masuk_s2'			=> $tahunmasuks2
			);
		
		// var_dump($data);exit();

		// cek ketersediaan
		$cek = $this->temph_model->getdetail('tbl_form_pmb','nomor_registrasi',$doc)->num_rows();
		if ($cek == 0) {
			$this->db->insert('tbl_form_pmb', $data);
		} else {
			$this->db->where('nomor_registrasi', $doc);
			$this->db->update('tbl_form_pmb', $data);
		}
		echo "<script>alert('Berhasil');</script>";
		redirect(base_url('home/load_form/'.$log),'refresh');
	}

	function load_form($log)
	{
		$data['detl'] = $this->temph_model->getdetail('tbl_form_pmb','user_input',$log)->row_array();
		$data['page'] = "v_load_detailmaba";
		$this->load->view('template', $data);
	}

}