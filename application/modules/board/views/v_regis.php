<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <div class="block">
      <div class="section-title col-sm-12">
        <h2>Pendaftaran Mahasiswa Baru</h2>
        <h6>Silahkan isi formulir dibawah ini untuk mendaftarkan akun</h6>
        <p></p>
      </div>
      <form action="<?php echo base_url(); ?>board/register/registering" method="post">
        <div class="form-group col-md-6">
          <input type="text" class="form-control" name="nm_dpn" placeholder="Nama Depan" required>
        </div>
        <div class="form-group col-md-6">
          <input type="text" class="form-control" name="nm_blk" placeholder="Nama Belakang">
        </div>
        <!-- <div class="form-group col-md-12">
          <input type="text" class="form-control" name="nik" placeholder="NIK" required>
        </div> -->
        <div class="form-group col-md-12">
          <input type="email" class="form-control" name="email" placeholder="E-mail" required>
        </div>
        <div class="form-group col-md-12">
          <input type="password" class="form-control" name="password" placeholder="Password" required>
        </div>
        <div class="form-group col-md-12">
          <input type="text" class="form-control" name="tlp" placeholder="Telepon" required>
        </div>
        <div class="form-group col-md-12">
          <button type="submit" class="btn" style="background:#1E90FF;color:white;"><i class="fa fa-check"></i> Submit</button>
          <button type="reset" class="btn" style="background:#FF1000;color:white;"><i class="fa fa-times"></i> Reset</button>
          <br><br>
          <a href="<?php echo base_url('board/login'); ?>" title="">Sudah mendaftar ?</a>
        </div>

      </form>
    </div>
  </div><!-- .col-md-7 close -->
  <!-- .col-md-5 close -->
</div>