<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Bootstrap Example</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    
    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>
</head>
<body>
 
	<div class="container">
		<br><br><br><br>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="panel panel-success">
				    <div class="panel-heading">
				    	<center>
						    <i class="fa fa-check-circle fa-5x" ></i>
							<h3>Registrasi Berhasil!</h3>
						</center>
				    </div>
				    <div class="panel-body"><p>Mohon cek spam pada e-mail anda. Username dan password akun anda akan dikirimkan oleh UBJ dalam waktu 24 jam. <br>Terimakasih.<br><br>Salam, <br>Panitia PMB UBJ.</p></div>
				    <div class="panel-footer"><a href="<?php echo base_url('board/register'); ?>"><i class="fa fa-arrow-left"></i> Kembali</a></div>
			  	</div>
			  	<center>
			  		<a href="http://ubharajaya.ac.id" title=""><small>&copy Universitas Bhayangkara Jakarta Raya</small></a>	
			  	</center>
			</div>
	  	</div>
	</div>

</body>
</html>
