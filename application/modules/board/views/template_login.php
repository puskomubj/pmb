<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Registration UBJ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css">
    
    <!-- Js -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/min/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.counterup.js"></script>

    <script src="<?php echo base_url();?>assets/js/main.js"></script>


  </head>
  <body>



    <!-- Header Start -->
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- header Nav Start -->
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                  <img src="<?php echo base_url();?>assets/img/logo.png" alt="Logo">
                </a>
              </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url();?>main/how_to">How To</a></li>
                    <li><a href="#">e-Registration</a></li>
                    <li><a href="<?php echo base_url();?>main/term">Term & Condition</a></li>
                    <li><a target="blank" href="http://ubharajaya.ac.id/wp-ubj/kontak/">Contact</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </div>
        </div>
      </div>
    </header><!-- header close -->
        
    <!-- Wrapper Start -->
    <section id="intro">
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-12" style="border-right:1px solid hsla(100, 5%, 80%,100)">
            <div class="block" >
              <div class="section-title col-sm-12">
                <h2>Silahkan Log-In</h2>
                <h6>Gunakan akun yang telah dikirim ke e-mail anda</h6>
                <p></p>
              </div>
              <form action="<?php echo base_url('main/chk_log'); ?>" method="post">
                <div class="form-group col-md-12">
                  <input type="text" class="form-control" name="username" placeholder="Username" required>
                </div>
                <div class="form-group col-md-12">
                  <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group col-md-4">
                  <button type="submit" class="btn" style="background:#DAA520;color:white;"><i class="fa fa-sign-in"></i> Login</button>
                  <br><br>
                  <a href="#forget" data-toggle="modal">Forget Password ?</a>
                </div>
              </form>
              <div class="section-title col-sm-12">
                <p></p>
              </div>
            </div>
          </div><!-- .col-md-5 close -->
        </div>
      </div>
    </section>

    
    <!-- footer Start -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="footer-manu">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url();?>main/how_to">How To</a></li>
                <li><a href="#">e-Registration</a></li>
                <li><a href="<?php echo base_url();?>main/term">Term & Condition</a></li>
                <li><a target="blank" href="http://ubharajaya.ac.id/wp-ubj/kontak/">Contact</a></li>
              </ul>
            </div>
            <p>Copyright &copy; Crafted by <a href="https://dcrazed.com/">Dcrazed</a>.</p>
          </div>
        </div>
      </div>
    </footer>

    <div class="modal fade" id="forget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Masukan E-mail Anda</h4>
              <small>Diperlukan untuk proses perubahan password</small>
          </div>
          <form class ='form-horizontal' action="<?php echo base_url(); ?>main/forgetpass/" method="post">
            <div class="modal-body">  
              <div class="form-group col-md-12">
                <input type="text" class="form-control" placeholder="Masukan E-mail Anda" required>
              </div>
              <div class="form-group col-md-12">
                <input type="password" class="form-control" placeholder="Masukan Telepon Anda" required>
              </div>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <input type="submit" class="btn btn-success" value="Kirim"/>
            </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div>
            
            
  <!-- <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script> -->
            
    
    </body>
</html>