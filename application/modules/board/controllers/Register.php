<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->dbsia = $this->load->database('pmb', TRUE);
	}

	public function index()
	{
		$data['page'] = 'v_regis';
		$this->load->view('template',$data);
	}

	function registering()
	{
		// cek gelombang
		$nm_dpn = strtoupper($this->input->post('nm_dpn'));
		$nm_blk = strtoupper($this->input->post('nm_blk'));
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		$tlp = $this->input->post('tlp');

		$cek = $this->temph_model->cek_regist($email,$tlp)->result();
		
		if (count($cek) == 0) {
			$gel = $this->dbsia->query('SELECT * from tbl_gelombang_pmb where status = 1')->row()->gelombang;
			$tg = date('Y');
			$pecah = substr($tg, 2,4);
			$no = $this->db->query("SELECT * from tbl_regist where userid like '".$pecah.$gel."%'")->num_rows();			
			$c = ($no+1);
			
			if($no <= 9){
	            $doc = $pecah.$gel."0000".$c;
	        }elseif($no <= 99){
	            $doc = $pecah.$gel."000".$c;
	        }elseif($no <= 999){
	            $doc = $pecah.$gel."00".$c;
	        }elseif($no <= 9999){
	            $doc = $pecah.$gel."0".$c;
	        }elseif($no <= 99999){
	            $doc = $pecah.$gel.$c;
	        }

	        $this->send_mail($email,$nm_dpn,$doc);

			$data = array(
				'nm_depan'		=> $nm_dpn ,
				'nm_belakang'	=> $nm_blk,
				// 'nik'			=> $this->input->post('nik'),
				'password'		=> $pass,
				'email'			=> $email,
				'tlp'			=> $tlp,
				'status'		=> 0,
				'regis_date'	=> date('Y-m-d H:i:s'),
				'userid'		=> $doc
				);
			$this->db->insert('tbl_regist', $data);


			redirect(base_url('board/register/success_page'),'refresh');
		} else {
			echo "<script>alert('Email/Nomor Telepon sudah terdaftar');document.location.href='".base_url('board/register')."'</script>";
		}
	}

	function success_page()
	{
		$this->load->view('success_page');
	}

	function send_mail($get_mail,$nama,$userid){		

		$admin = 'nurfan@ubharajaya.ac.id';
		//$email = $get_mail.','.$admin;
		//$email = $get_mail;

		//generate key
		$hash = NULL;
        $n = 20; // jumlah karakter yang akan di bentuk.
        $chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvqxyz0123456789";
        for ($i = 0; $i < $n; $i++) {
            $rIdx = rand(1, strlen($chr));
            $hash .=substr($chr, $rIdx, 1);
        }
        $key = $hash.date('ymdHis');
        //end generate key

        //insert tbl_aktivasi
        $data_mail = array('email' => $get_mail, 
        					'userid' => $userid,
        					'key' => $key,
        					'flag' => 0,
        					'send_date' => date('Y-m-d H:i:s') 
        					);

        $this->db->insert('tbl_aktivasi', $data_mail);
        //end insert tbl_aktivasi

		$isi = '<p>Hai '.$nama.',</p>
				<p>Terimakasih telah mendaftarkan diri pada aplikasi Penerimaan Mahasiswa Baru Universitas Bhayangkara Jakarta Raya.</p>
				<p>Untuk untuk memverifikasi dan login anda bisa menggunakan LINK di bawah ini.</p>'.anchor('http://172.16.2.65/registration/aktivasi/akun/'.$key).'
				</br>
				<p>Terima Kasih</p>';

		//$isi = $this->load->view('forgot_mail');
		
		$judul='Aktivasi Akun Calon Mahasiswa';

		$config = Array(
	        'protocol' => 'smtp',
	        'smtp_host' => 'ssl://smtp.gmail.com',
	        'smtp_port' => 465,
	        //'smtp_user' => 'nurfan.test@gmail.com', //email id
	        //'smtp_pass' => 'KIKi270293',// password
	        'smtp_user' => 'it@ubharajaya.ac.id', //email id
	        'smtp_pass' => 'Pusk0mUBJ',
	        'mailtype'  => 'html', 
	        'charset'   => 'iso-8859-1'
	    );
	    
	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");

	    $this->email->from('IT@ubharajaya.ac.id','Universitas Bhayangkara Jakarta Raya');
	    $this->email->to($get_mail); // email array
	    $this->email->subject($judul);   
	    $this->email->message($isi);

	    $result = $this->email->send();
	}


	function generateRandomString() 
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}



}

/* End of file Regist.php */
/* Location: ./application/modules/main/controllers/Regist.php */