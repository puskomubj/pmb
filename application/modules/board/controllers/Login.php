<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$data['page'] = 'v_login';
		$this->load->view('template',$data);
	}

	function vrif_login($usr)
	{
		$chk = $this->temph_model->vrify_log($usr)->result();
		// var_dump($chk);exit();
		if (count($chk) == 1) {
			foreach ($chk as $key) {
				$sess['user'] = $key->email;
				$sess['idus'] = $key->userid;
				$sess['pass'] = $key->password;
				$this->session->set_userdata('sess_log_maba', $sess);

				// update verif login
				$data = array('vrif_log' => 1, 'date_vrif' => date('Y-m-d H:i:s'));
				$this->db->where('userid', $key->userid);
				$this->db->update('tbl_user_login',$data);

				redirect(base_url('home'),'refresh');
			}
		} else {
			echo "<script>alert('Menyingkir Kau Penyusup!');history.go(-1);</script>";
		}
		
	}

	function chk_log()
	{
		$usr = $this->input->post('username');
		$pwd = $this->input->post('password');
		
		// cek username & password
		$c_akun = $this->temph_model->cek_login($usr,$pwd)->result();

		//load data user
		$userdata =$this->db->where('email',$usr)->get('tbl_regist',1)->result();

		// cek ketersedian akun
		//$ada_akun = $this->temph_model->cek_akun($usr,$pwd)->row();

		//var_dump($ada_akun);die();

		//if ($ada_akun->mail != NULL) {
			if (count($c_akun) == 1) {
		
				foreach ($userdata as $key) {
					$sess['username'] = $key->email;
					$sess['userid'] = $key->userid;
					$sess['password'] = $key->password;
					$sess['nm_depan'] = $key->nm_depan;
					$sess['nm_belakang'] = $key->nm_belakang;
					$this->session->set_userdata('sess_login', $sess);
					redirect(base_url('home'),'refresh');
				}

			} elseif(count($c_akun) == 0) {
				echo "<script>alert('Gagal Login, Pastikan kembali Username dan Password anda sudah benar.');history.go(-1);</script>";exit();
			}
		// }else{
		// 	echo "<script>alert('Mohon Verifikasi terlebih dahulu akun anda melalui email yang anda daftarkan.');history.go(-1);</script>";exit();
		// }
		// echo "<script>alert('Gagal Login, Pastikan anda telah mendaftar pada aplikasi ini.');history.go(-1);</script>";exit();
	}

	function out()
	{
		$this->session->unset_userdata('sess_log_maba');
		redirect(base_url('board/login'),'refresh');
	}

	function forgetpass()
	{
		$mail =  $this->input->post('email');
		$phon = $this->input->post('phone');

		$cek = $this->temph_model->chk_account($mail,$phon);
		if (count($cek->result()) > 0) {
			// send e-mail
			$load['data'] = $this->temph_model->load_account($mail)->row();
			$lawn = $this->load->view('v_mail_fgpass', $load, TRUE);
			$this->load->library('email');
			$judul='Universitas Bhayangkara Jakarta Raya';
			$config = array(
	            'protocol' 	=> 'smtp',
	            'smtp_host' => 'ssl://smtp.gmail.com',
	            'smtp_port' => 465,
	            'smtp_user' => 'hilmawan@ubharajaya.ac.id',    	//email id
	            'smtp_pass' => '192735**',            			// password
	            'mailtype'  => 'html',
	            'charset'   => 'iso-8859-1',
	            'validation'=> TRUE
	        );
	       	$this->email->initialize($config);
	        $this->email->set_newline("\r\n");
	        $this->email->from('hilmawan@ubharajaya.ac.id','hilmawan test');
	        $this->email->to($mail); // email array
	        $this->email->subject($judul);  
	        $this->email->message($lawn);
	        $result = $this->email->send();
	        echo "<script>alert('Mohon cek spam e-mail Anda!');history.go(-1);</script>";
		} else {
			echo "<script>alert('Akun Tidak Ditemukan Wahai Pendusta!');history.go(-1);</script>";
		}
		
	}

}

/* End of file Login.php */
/* Location: ./application/modules/main/controllers/Login.php */