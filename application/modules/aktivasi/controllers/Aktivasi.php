<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktivasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function akun($key)
	{
		$cek_key 	= $this->db->where('key',$key)->get('tbl_aktivasi',1);
		$count 		= $cek_key->num_rows();

		if ($count == 1) {
			$userid = $cek_key->row()->userid;
			$user =  $this->db->where('userid',$userid)->get('tbl_regist',1)->row();

			$session['username']	= trim($user->email);
			$session['userid'] 		= trim($user->userid);
			$session['nm_depan'] 	= trim($user->nm_depan);
			$session['nm_belakang'] = trim($user->nm_belakang);

			$this->session->set_userdata('sess_login',$session);
			$this->session->set_userdata('userid',$session['userid']);
			
			$user_login = array(
							'email'				=> $user->email, 
							'password' 			=> md5($user->password), 
							'password_plain' 	=> $user->password, 
							'userid' 			=> $user->userid
							);
			
			$this->db->insert('tbl_user_login', $user_login);
			redirect('home','refresh');			
		}else{
			echo "kesalahan ";
		}
	}

}

/* End of file Aktivasi.php */
/* Location: ./application/controllers/Aktivasi.php */