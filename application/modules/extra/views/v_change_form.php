<script type="text/javascript">
$(function(){
    $(".showpassword").each(function(index,input) {
        var $input = $(input);
        $('<label class="showpasswordlabel"/>').append(
            $("<input type='checkbox' class='showpasswordcheckbox' />").click(function() {
                var change = $(this).is(":checked") ? "text" : "password";
                var rep = $("<input type='" + change + "' />")
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
                $input.remove();
                $input = rep;
             })
        ).append($("<span/>").text("Show password")).insertAfter($input);
    });
});

function checkPass()
{
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    if(pass1.value == pass2.value){
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        document.getElementById('save').disabled = false;
        message.innerHTML = "Passwords Match!"

    }else{
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        document.getElementById('save').disabled = true;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  

</script>

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <div class="block">
      <div class="section-title col-sm-12">
        <h2>Ganti Password</h2>
        <h6>Silahkan isi form di bawah untuk mengganti password</h6>
        <p></p>
      </div>
      <form action="<?php echo base_url(); ?>extra/change_pass/change" method="post">
        <div class="form-group col-md-12">
          <input type="password" class="form-control" name="old_pass" placeholder="Password Lama" required>
        </div>
        <div class="form-group col-md-12">
          <input type="password" class="form-control" name="new_pass" id="pass1" placeholder="Password Baru" required>
        </div>
        <div class="form-group col-md-12">
          <input type="password" class="form-control" name="re_pass" id="pass2" onkeyup="checkPass(); return false;" placeholder="Ulangi Password Baru" required>
          <span id="confirmMessage" class="confirmMessage"></span>
        </div>
        <div class="form-group col-md-12">
          <button type="submit" class="btn" id="save" style="background:#1E90FF;color:white;"><i class="fa fa-check"></i> Submit</button>
          <button type="reset" class="btn" style="background:#FF1000;color:white;"><i class="fa fa-times"></i> Reset</button>
          <br><br>
        </div>

      </form>
    </div>
  </div><!-- .col-md-7 close -->
  <!-- .col-md-5 close -->
</div>

<script type="text/javascript">

</script>

